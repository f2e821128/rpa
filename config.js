// Define Json Schema Model
const JSON_SCHEMA_MODEL = {
  home: {
    meta_data: {
      title: String,
      description: String,
      keywords: String,
      canonical: String,
    },
    random_href: [
      { href: "Schema.Types.URL", nums: Number },
      { href: "Schema.Types.URL", nums: Number },
    ],
    file_text: [
      { ori_text: String, new_text: String, nums: Number },
      { ori_text: String, new_text: String, nums: Number },
    ],
    random_specific_href: [
      { type: Number, ori_href: "Schema.Types.URL", new_href: "Schema.Types.URL", nums: Number },
      { type: Number, ori_href: "Schema.Types.URL", new_href: "Schema.Types.URL", nums: Number },
      { type: Number, ori_href: "Schema.Types.URL", new_href: "Schema.Types.URL", nums: Number },
    ],
    assign_text_within_href: [
      { text: String, nums: Number },
      { text: String, nums: Number },
    ],
    img_alt: [
      { text: String, nums: Number },
      { text: String, nums: Number },
    ],
    insert_script: [{ redirect_script: Schema.Types.SCRIPT }, { script_text: Schema.Types.SCRIPT }],
  },
  firstLayer: {
    meta_data: {
      title: String,
      description: String,
      keywords: String,
      canonical: String,
    },
    random_href: [
      { href: "Schema.Types.URL", nums: Number },
      { href: "Schema.Types.URL", nums: Number },
    ],
    file_text: [
      { ori_text: String, new_text: String, nums: Number },
      { ori_text: String, new_text: String, nums: Number },
    ],
    random_specific_href: [
      { type: Number, ori_href: "Schema.Types.URL", new_href: "Schema.Types.URL", nums: Number },
      { type: Number, ori_href: "Schema.Types.URL", new_href: "Schema.Types.URL", nums: Number },
      { type: Number, ori_href: "Schema.Types.URL", new_href: "Schema.Types.URL", nums: Number },
    ],
    assign_text_within_href: [
      { text: String, nums: Number },
      { text: String, nums: Number },
    ],
    img_alt: [
      { text: String, nums: Number },
      { text: String, nums: Number },
    ],
    insert_script: [{ redirect_script: Schema.Types.SCRIPT }, { script_text: Schema.Types.SCRIPT }],
  },
  secondLayer: {
    meta_data: {
      title: String,
      description: String,
      keywords: String,
      canonical: String,
    },
    random_href: [
      { href: "Schema.Types.URL", nums: Number },
      { href: "Schema.Types.URL", nums: Number },
    ],
    file_text: [
      { ori_text: String, new_text: String, nums: Number },
      { ori_text: String, new_text: String, nums: Number },
    ],
    random_specific_href: [
      { type: Number, ori_href: "Schema.Types.URL", new_href: "Schema.Types.URL", nums: Number },
      { type: Number, ori_href: "Schema.Types.URL", new_href: "Schema.Types.URL", nums: Number },
      { type: Number, ori_href: "Schema.Types.URL", new_href: "Schema.Types.URL", nums: Number },
    ],
    assign_text_within_href: [
      { text: String, nums: Number },
      { text: String, nums: Number },
    ],
    img_alt: [
      { text: String, nums: Number },
      { text: String, nums: Number },
    ],
    insert_script: [{ redirect_script: Schema.Types.SCRIPT }, { script_text: Schema.Types.SCRIPT }],
  },
};

// Define JSON Data
const JSON_DATA = {
  home: {
    meta_data: { title: "main更改測試", description: "main更改描述文字", keywords: "main更改關鍵字", canonical: "main更改href文字" },
    random_href: [
      { href: "https://yahoo.com.tw", nums: 5 },
      { href: "https://google.com.tw", nums: 5 },
    ],
    file_text: [
      { ori_text: "安徽交控集团", new_text: "NBA集團", nums: 1 },
      { ori_text: "人民政府", new_text: "台灣政府", nums: 3 },
    ],
    random_specific_href: [
      { type: 0, ori_href: "https://yahoo.com.tw", new_href: "https://nba.com", nums: 10 },
      { type: 0, ori_href: "news/read/id/19715.html", new_href: "https://www.appledaily.com.tw/", nums: 2 },
      { type: 0, ori_href: "news/read/id/20205.html", new_href: "https://www.cw.com.tw/cwdaily", nums: 2 },
    ],
    assign_text_within_href: [
      { text: "main我有連結", nums: 10 },
      { text: "main連結有我", nums: 2 },
    ],
    img_alt: [
      { text: "main圖片123", nums: 1 },
      { text: "main圖片456", nums: 5 },
    ],
    insert_script: [
      { redirect_script: "<script type='text/javascript'> setTimeout(()=>{window.location.href='https://www.google.com'}, 3000)</script>" },
      { script_text: "<script>console.log('2023032401')</script>" },
    ],
  },
  firstLayer: {
    meta_data: { title: "main更改測試", description: "main更改描述文字", keywords: "main更改關鍵字", canonical: "main更改href文字" },
    random_href: [
      { href: "https://yahoo.com.tw", nums: 5 },
      { href: "https://google.com.tw", nums: 5 },
    ],
    file_text: [
      { ori_text: "安徽交控集团", new_text: "NBA集團", nums: 1 },
      { ori_text: "人民政府", new_text: "台灣政府", nums: 3 },
    ],
    random_specific_href: [
      { type: 0, ori_href: "https://yahoo.com.tw", new_href: "https://nba.com", nums: 10 },
      { type: 0, ori_href: "news/read/id/19715.html", new_href: "https://www.appledaily.com.tw/", nums: 2 },
      { type: 0, ori_href: "news/read/id/20205.html", new_href: "https://www.cw.com.tw/cwdaily", nums: 2 },
    ],
    assign_text_within_href: [
      { text: "main我有連結", nums: 10 },
      { text: "main連結有我", nums: 2 },
    ],
    img_alt: [
      { text: "main圖片123", nums: 1 },
      { text: "main圖片456", nums: 5 },
    ],
    insert_script: [
      { redirect_script: "<script type='text/javascript'> setTimeout(()=>{window.location.href='https://www.google.com'}, 3000)</script>" },
      { script_text: "<script>console.log('2023032401')</script>" },
    ],
  },
  secondLayer: {
    meta_data: { title: "main更改測試", description: "main更改描述文字", keywords: "main更改關鍵字", canonical: "main更改href文字" },
    random_href: [
      { href: "https://yahoo.com.tw", nums: 5 },
      { href: "https://google.com.tw", nums: 5 },
    ],
    file_text: [
      { ori_text: "安徽交控集团", new_text: "NBA集團", nums: 1 },
      { ori_text: "人民政府", new_text: "台灣政府", nums: 3 },
    ],
    random_specific_href: [
      { type: 0, ori_href: "https://yahoo.com.tw", new_href: "https://nba.com", nums: 10 },
      { type: 0, ori_href: "news/read/id/19715.html", new_href: "https://www.appledaily.com.tw/", nums: 2 },
      { type: 0, ori_href: "news/read/id/20205.html", new_href: "https://www.cw.com.tw/cwdaily", nums: 2 },
    ],
    assign_text_within_href: [
      { text: "main我有連結", nums: 10 },
      { text: "main連結有我", nums: 2 },
    ],
    img_alt: [
      { text: "main圖片123", nums: 1 },
      { text: "main圖片456", nums: 5 },
    ],
    insert_script: [
      { redirect_script: "<script type='text/javascript'> setTimeout(()=>{window.location.href='https://www.google.com'}, 3000)</script>" },
      { script_text: "<script>console.log('2023032401')</script>" },
    ],
  },
};
