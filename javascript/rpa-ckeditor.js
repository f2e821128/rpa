CKEDITOR.replace("rpa-editor-1");

CKEDITOR.instances["rpa-editor-1"].on("change", () => {
  onliveChangeWithBtn("ads-module-04-form-body");
});

CKEDITOR.on("instanceCreated", function (e) {
  e.editor.on("change", (event) => {
    onliveChangeWithBtn("ads-module-04-form-body");
  });
});
