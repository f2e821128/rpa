function appendSelect(appendDiv, ...args) {
  if (args.length < 2) {
    return;
  }

  let label = `<li class="d-flex text-nowrap rpa-badge rpa-badge-light-purple text-white">`;
  for (let i = 0; i < args.length; i++) {
    if (i == 0) {
      label += $(`#${args[i]}`).children().text();
    } else {
      label += ` ~ `;
      label += $(`#${args[i]}`).children().text();
    }
  }
  label += `</li>`;
  $(`#${appendDiv}`).append(label);
}
