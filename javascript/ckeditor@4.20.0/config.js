/**
 * @license Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function (config) {
  // Define changes to default configuration here.
  // For complete reference see:
  // https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_config.html

  // Set the most common block elements.
  // config.format_tags = "p;h1;h2;h3;pre;div";

  // Simplify the dialog windows.
  config.removeDialogTabs = "image:advanced;link:advanced,url";

  config.width = "100%";
  config.height = "500px";
  config.resize_enabled = false;
  config.uiColor = "#ccc4e6";

  // The toolbar groups arrangement, optimized for two toolbar rows.
  config.toolbarGroups = [
    { name: "paragraph", groups: ["list", "indent", "blocks", "align", "bidi", "paragraph"] },
    { name: "insert", groups: ["insert"] },
    { name: "styles", groups: ["styles"] },
    { name: "colors" },
  ];

  // Remove some buttons provided by the standard plugins, which are
  // not needed in the Standard(s) toolbar.
  config.removeButtons =
    "Source,Save,NewPage,Preview,Print,Templates,Cut,Copy,Paste,PasteText," +
    "PasteFromWord,Redo,Undo,Find,Replace,SelectAll,Scayt,Form,TextField,Radio,Checkbox," +
    "Textarea,Button,Select,ImageButton,HiddenField,Superscript,Subscript,Strike,Underline,Italic," +
    "Bold,RemoveFormat,NumberedList,Outdent,BulletedList,Indent,CreateDiv,Blockquote,BidiLtr,BidiRtl," +
    "Language,JustifyBlock,Anchor,Unlink,Link,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak," +
    "Iframe,Format,Styles,Font,About,ShowBlocks,Maximize";

  config.extraPlugins = "justify, font, colorbutton, panelbutton";
};
