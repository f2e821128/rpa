const goToTopScroll = function (element) {
  if ($(element).scrollTop() >= 1) {
    $(".rpa-goto-top").addClass("active");
  } else {
    $(".rpa-goto-top").removeClass("active");
  }
};

function goToTop(element) {
  $(element).scrollTop(0);
}

function rpaScrollDetect(element) {
  $(element).on("scroll", function (e) {
    goToTopScroll(element);
  });
}
