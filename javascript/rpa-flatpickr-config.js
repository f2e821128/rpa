const datetimepicker = ["flatpickr-datetime-input-01", "flatpickr-datetime-input-02", "flatpickr-datetime-input-03", "flatpickr-datetime-input-04"];
const dateRangePicker = ["flatpickr-range-input-01", "flatpickr-range-input-02"];
const datePicker = ["short_link_begin_date", "short_link_end_date"];
const dateSinglePicker = ["flatpickr-single-input-01"];

const flatpickr_range_config = {
  enableTime: false, // 開啟時間選項
  dateFormat: "Y-m-d", // 日期時間格式
  mode: "range", // 是否要區間
  locale: {
    rangeSeparator: " ~ ", // 更換日期區間的表達式
  },
  static: true,
};

const flatpickr_datetime_config = {
  enableTime: true, // 開啟時間選項
  dateFormat: "Y-m-d H:i", // 日期時間格式
  static: true,
};

const flatpickr_date_config = {
  enableTime: false, // 開啟時間選項
  dateFormat: "Y-m-d", // 日期時間格式
  static: true,
};

datetimepicker.forEach((elementID) => {
  $(`#${elementID}`).flatpickr(flatpickr_datetime_config);
});

dateRangePicker.forEach((elementID) => {
  $(`#${elementID}`).flatpickr(flatpickr_range_config);
});

datePicker.forEach((elementID) => {
  $(`#${elementID}`).flatpickr(flatpickr_date_config);
});

dateSinglePicker.forEach((elementID) => {
  $(`#${elementID}`).flatpickr(flatpickr_date_config);
});
