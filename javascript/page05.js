// Function for Env Create Table
let exchangeRulesForm = `
        <div class="row m-0 mb__14" data-rules-form="replace_keyword">
            <div class="col-md-12 p-0">
                <span class="rpa-fs-14 rpa-text-purple rpa-fw-400">替代規則</span>
                <form action="#" class="rpa-border-purple p__10 mt__4" data-form="replace_keyword">
                    <!-- Config Row -->
                    <div class="row m-0 rpa-gap__10 flex-lg-nowrap mb__14">
                        <!-- Select Env -->
                        <div class="col-lg-5 col-md-12 p-0 flex-lg-fill">
                            <span class="rpa-fs-14 rpa-text-purple rpa-fw-400 mr__10"> 替代環境 </span>
                            <div class="input-group d-flex align-items-center mt__14">
                                <div class="form-check d-flex align-items-center rpa-radio-purple d-none">
                                    <input type="radio" value="全部" id="exchange-env-full-replace_keyword" name="exchange-env-replace_keyword"  />
                                    <label class="rpa-fs-14 rpa-text-purple rpa-fw-400" for="exchange-env-full-replace_keyword"> 全部 </label>
                                </div>
                                <div class="form-check d-flex align-items-center rpa-radio-purple">
                                    <input type="radio" value="首頁" id="exchange-env-home-replace_keyword" name="exchange-env-replace_keyword" checked/>
                                    <label class="rpa-fs-14 rpa-text-purple rpa-fw-400" for="exchange-env-home-replace_keyword"> 首頁 </label>
                                </div>
                                <div class="form-check d-flex align-items-center rpa-radio-purple">
                                    <input type="radio" value="其他" id="exchange-env-other-replace_keyword" name="exchange-env-replace_keyword" />
                                    <label class="rpa-fs-14 rpa-text-purple rpa-fw-400" for="exchange-env-other-replace_keyword"> 其他 </label>
                                </div>
                            </div>
                        </div>
                        <!-- Dropdown Select Rules & Combination -->
                        <div class="col-lg-7 col-md-12 p-0 flex-lg-fill">
                            <span class="rpa-fs-14 rpa-text-purple rpa-fw-400"> 設定規則 </span>
                            <div class="d-block mt__5">
                                <div class="rpa-dropdown purple">
                                    <div class="rpa-dropdown-selected" id="dropdown-rules-replace_keyword" onclick="rpaDropdownFunc('dropdown-rules-replace_keyword')">
                                        <span class="rpa-dropdown-selected-text" id="dropdown-rules-replace_keyword-span"> 規則 </span>
                                    </div>
                                    <ul class="rpa-dropdown-list os-viewport-native-scrollbars-invisible" data-container="dropdown-rules-replace_keyword" id="dropdown-rules-replace_keyword-ul">
                                        <li class="rpa-dropdown-list-item" value="" onclick="appendItem('replace_keyword', 2, null)">特殊連結取代</li>
                                        <li class="rpa-dropdown-list-item" value="" onclick="appendItem('replace_keyword', 1, null)">a href 標籤連結取代</li>

                                        <li class="rpa-dropdown-list-item" value="" onclick="appendItem('replace_keyword', null, 0, 1)">文字組合一 &nbsp; (主詞和副詞)</li>
                                        <li class="rpa-dropdown-list-item" value="" onclick="appendItem('replace_keyword', null, 0, 2)">文字組合二 &nbsp; (主詞和長尾詞)</li>
                                        <li class="rpa-dropdown-list-item" value="" onclick="appendItem('replace_keyword', null, 0, 1, 2)">文字組合三 &nbsp; (主詞、副詞、長尾詞)</li>
                                        <li class="rpa-dropdown-list-item" value="" onclick="appendItem('replace_keyword', null, 'blank')">文字組合四 &nbsp; (純文字組合)</li>

                                        <li class="rpa-dropdown-list-item" value="" onclick="appendItem('replace_keyword', 0, 0, 1)">a href 標籤文字取代 &nbsp; (主詞和副詞)</li>
                                        <li class="rpa-dropdown-list-item" value="" onclick="appendItem('replace_keyword', 0, 0, 2)">a href 標籤文字取代 &nbsp; (主詞和長尾詞)</li>
                                        <li class="rpa-dropdown-list-item" value="" onclick="appendItem('replace_keyword', 0, 0, 1, 2)">a href 標籤文字取代 &nbsp; (主詞、副詞、長尾詞)</li>
                                        <li class="rpa-dropdown-list-item" value="" onclick="appendItem('replace_keyword', 0, 'blank')">a href 標籤文字取代 &nbsp; (純文字組合)</li>

                                        <li class="rpa-dropdown-list-item" value="" onclick="appendItem('replace_keyword', 3, 0, 1)">圖片 alt 替代 &nbsp; (主詞和副詞)</li>
                                        <li class="rpa-dropdown-list-item" value="" onclick="appendItem('replace_keyword', 3, 0, 2)">圖片 alt 替代 &nbsp; (主詞和長尾詞)</li>
                                        <li class="rpa-dropdown-list-item" value="" onclick="appendItem('replace_keyword', 3, 0, 1, 2)">圖片 alt 替代 &nbsp; (主詞、副詞、長尾詞)</li>
                                        <li class="rpa-dropdown-list-item" value="" onclick="appendItem('replace_keyword', 3, 'blank')">圖片 alt 替代 &nbsp; (純文字組合)</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-1 col-md-12 p-0 flex-lg-fill">
                          <div class="d-grid align-items-center h-100">
                              <div class="rpa-btn rpa-btn-lg rpa-btn-red text-white" onclick="deleteRule('replace_keyword')">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16">
                                      <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"></path>
                                  </svg>
                              </div>
                          </div>
                      </div>
                    </div>
                </form>
            </div>
        </div>`;

// 文字組合與手動輸入(Done)
let textInputBox = `
          <div class="row m-0 flex-lg-nowrap mb__14 rpa-gap__10" data-form-append="replace_keyword">
              <div class="col-lg-4 col-md-12 p-0 flex-lg-fill">
                  <span class="rpa-fs-14 rpa-text-purple rpa-fw-400"> 替代文字 </span>
                  <div class="input-group mt__4">
                      <input type="text" class="form-control shadow-none rpa-input-purple rpa-fs-14 rpa-fw-400" name="replace_text-replace_keyword" data-input="replace_text-replace_keyword" placeholder="替代文字" />
                  </div>
              </div>
              <div class="col-lg-4 col-md-12 p-0 flex-lg-fill">
                  <span class="rpa-fs-14 rpa-text-purple rpa-fw-400"> 原本文字 </span>
                  <div class="input-group mt__4">
                      <input type="text" class="form-control shadow-none rpa-input-purple rpa-fs-14 rpa-fw-400" name="signed_text-replace_keyword" data-input="signed_text-replace_keyword" placeholder="原本文字" />
                  </div>
              </div>
              <div class="col-lg-4 col-md-12 p-0 flex-lg-fill">
                  <span class="rpa-fs-14 rpa-text-purple rpa-fw-400"> 取代數量 </span>
                  <div class="input-group mt__4">
                      <input type="text" class="form-control shadow-none rpa-input-purple rpa-fs-14 rpa-fw-400" name="nums-replace_keyword" data-input="nums-replace_keyword" placeholder="取代數量..." />
                  </div>
              </div>
          </div>`;

// 文字組合非手動輸入(Done)
let textDisabledInputBox = `
          <div class="row m-0 mb__14 flex-lg-nowrap rpa-gap__10" data-form-append="replace_keyword">
              <div class="col-lg-4 col-md-12 p-0 flex-lg-fill">
                  <span class="rpa-fs-14 rpa-text-purple rpa-fw-400"> 替代文字 </span>
                  <div class="input-group mt__4">
                      <input type="text" class="form-control shadow-none rpa-input-purple rpa-fs-14 rpa-fw-400" name="replace_text-replace_keyword" data-input="replace_text-replace_keyword" value="replace_combine_word" disabled>
                  </div>
              </div>
              <div class="col-lg-4 col-md-12 p-0 flex-lg-fill">
                  <span class="rpa-fs-14 rpa-text-purple rpa-fw-400"> 原本文字 </span>
                  <div class="input-group mt__4">
                      <input type="text" class="form-control shadow-none rpa-input-purple rpa-fs-14 rpa-fw-400" name="signed_text-replace_keyword" data-input="signed_text-replace_keyword" placeholder="原本文字">
                  </div>
              </div>
              <div class="col-lg-4 col-md-12 p-0 flex-lg-fill">
                  <span class="rpa-fs-14 rpa-text-purple rpa-fw-400"> 取代數量 </span>
                  <div class="input-group mt__4">
                      <input type="text" class="form-control shadow-none rpa-input-purple rpa-fs-14 rpa-fw-400" name="nums-replace_keyword" data-input="nums-replace_keyword" placeholder="取代數量...">
                  </div>
              </div>
          </div>`;

// a href 標題文字替代與手動輸入(Done)
let aHrefTextReplace = `
          <div class="row m-0 flex-nowrap mb__14 rpa-gap__10" data-form-append="replace_keyword">
            <div class="col-lg-6 col-md-12 p-0 flex-fill">
              <span class="rpa-fs-14 rpa-text-purple rpa-fw-400"> 替代文字 </span>
              <div class="input-group mt__4">
                <input type="text" class="form-control shadow-none rpa-input-purple rpa-fs-14 rpa-fw-400" name="signed_text-replace_keyword" data-input="signed_text-replace_keyword" value="">
              </div>
            </div>  
            <div class="col-lg-6 col-md-12 p-0 flex-fill">
              <span class="rpa-fs-14 rpa-text-purple rpa-fw-400"> 取代數量 </span>
              <div class="input-group mt__4">
                  <input class="form-control shadow-none rpa-input-purple rpa-fs-14 rpa-fw-400" type="text" name="nums-replace_keyword" id="nums-replace_keyword" placeholder="取代數量..." aria-label="取代數量" />
              </div>
            </div>  
          </div>`;

// a href 標題文字替代非手動輸入(Done)
let aHrefTextReplacWithDisable = `
        <div class="row m-0 flex-nowrap mb__14 rpa-gap__10" data-form-append="replace_keyword">
          <div class="col-lg-6 col-md-12 p-0 flex-fill">
            <span class="rpa-fs-14 rpa-text-purple rpa-fw-400"> 替代文字 </span>
            <div class="input-group mt__4">
              <input type="text" class="form-control shadow-none rpa-input-purple rpa-fs-14 rpa-fw-400" name="signed_text-replace_keyword" data-input="signed_text-replace_keyword" value="replace_combine_word" disabled>
            </div>
          </div>
          <div class="col-lg-6 col-md-12 p-0 flex-fill">
            <span class="rpa-fs-14 rpa-text-purple rpa-fw-400"> 取代數量 </span>
            <div class="input-group mt__4">
              <input class="form-control shadow-none rpa-input-purple rpa-fs-14 rpa-fw-400" type="text" name="nums-replace_keyword" id="nums-replace_keyword" placeholder="取代數量..." aria-label="取代數量" />
            </div>
          </div>
        </div>`;

// 圖片 alt 替代與手動輸入(Done)
let imagAltReplace = `
          <div class="row m-0 flex-lg-nowrap rpa-gap__10 mb__14" data-form-append="replace_keyword">
              <div class="col-lg-8 col-md-12 p-0 flex-fill">
                <span class="rpa-fs-14 rpa-text-purple rpa-fw-400"> 替代文字 </span>
                  <div class="input-group mt__4">
                      <input type="text" class="form-control shadow-none rpa-input-purple rpa-fs-14 rpa-fw-400" name="replace_text-replace_keyword" data-input="replace_text-replace_keyword" value="" placeholder="替代文字">
                  </div>
              </div>
              <div class="col-lg-4 col-md-12 p-0 flex-fill">
                  <span class="rpa-fs-14 rpa-text-purple rpa-fw-400"> 取代數量 </span>
                  <div class="input-group mt__4">
                      <input class="form-control shadow-none rpa-input-purple rpa-fs-14 rpa-fw-400" type="text" name="nums-replace_keyword" id="nums-replace_keyword" placeholder="取代數量..." aria-label="取代數量" />
                  </div>
              </div>
          </div>`;

// 圖片 alt 替代非手動輸入(Done)
let imagAltReplaceWithDisabled = `
          <div class="row m-0 flex-lg-nowrap rpa-gap__10 mb__14" data-form-append="replace_keyword">
              <div class="col-lg-8 col-md-12 p-0 flex-fill">
                    <span class="rpa-fs-14 rpa-text-purple rpa-fw-400"> 替代文字 </span>
                    <div class="input-group mt__4">
                        <input type="text" class="form-control shadow-none rpa-input-purple rpa-fs-14 rpa-fw-400" name="replace_text-replace_keyword" data-input="replace_text-replace_keyword" value="replace_combine_word" disabled>
                    </div>
              </div>
              <div class="col-lg-4 col-md-12 p-0 flex-fill">
                  <span class="rpa-fs-14 rpa-text-purple rpa-fw-400"> 取代數量 </span>
                  <div class="input-group mt__4">
                      <input class="form-control shadow-none rpa-input-purple rpa-fs-14 rpa-fw-400" type="text" name="nums-replace_keyword" id="nums-replace_keyword" placeholder="取代數量..." aria-label="取代數量" />
                  </div>
              </div>
          </div>`;

// a href 標題連結替代(Done)
let aHrefLinkReplace = `
          <div class="row m-0 mb__14" data-form-append="replace_keyword">
              <div class="col-md-12 p-0">
                  <div class="row m-0 rpa-gap__10 flex-lg-nowrap">
                      <div class="col-lg-10 col-md-12 p-0 flex-lg-fill">
                          <span class="rpa-fs-14 rpa-text-purple rpa-fw-400"> a href 標籤連結取代 </span>
                          <div class="input-group mt__4">
                          <input class="form-control shadow-none rpa-input-purple rpa-fs-14 rpa-fw-400" type="text" name="replace_href-replace_keyword" id="replace_href-replace_keyword" placeholder="連結取代..." aria-label="連結取代" />
                          </div>
                      </div>
                      <div class="col-lg-2 col-md-12 p-0 flex-lg-fill">
                          <span class="rpa-fs-14 rpa-text-purple rpa-fw-400"> 取代數量 </span>
                          <div class="input-group mt__4">
                              <input class="form-control shadow-none rpa-input-purple rpa-fs-14 rpa-fw-400" type="text" name="nums-replace_keyword" id="nums-replace_keyword" placeholder="取代數量..." aria-label="取代數量" />
                          </div>
                      </div>
                  </div>
              </div>
          </div>`;

// 特殊連結取代(Done)
let specialLinkReplace = `
          <div class="row m-0 flex-lg-nowrap rpa-gap__10 mb__14" data-form-append="replace_keyword">
              <div class="col-lg-5 col-md-12 p-0 flex-fill mb__10 ">
                  <span class="rpa-fs-14 rpa-text-purple rpa-fw-400 mr__10"> a href 標籤連結取代 </span>
                  <div class="input-group mt__14 d-flex align-items-center">
                    <div class="form-check d-flex align-items-center rpa-radio-purple">
                        <input type="radio" value="全部" id="special-link-replace-full-rules-replace_keyword" name="special-link-replace-replace_keyword" checked/>
                        <label class="rpa-fs-14 rpa-text-purple rpa-fw-400" for="special-link-replace-full-rules-replace_keyword"> 全部 </label>
                    </div>
                    <div class="form-check d-flex align-items-center rpa-radio-purple">
                        <input type="radio" value="部分" id="special-link-replace-part-rules-replace_keyword" name="special-link-replace-replace_keyword"  />
                        <label class="rpa-fs-14 rpa-text-purple rpa-fw-400" for="special-link-replace-part-rules-replace_keyword"> 部分 </label>
                    </div>
                  </div>
              </div>
              <div class="col-lg-7 col-md-12 p-0 flex-fill">
                  <span class="rpa-fs-14 rpa-text-purple rpa-fw-400"> 取代數量 </span>
                  <div class="input-group mt__4">
                  <input class="form-control shadow-none rpa-input-purple rpa-fs-14 rpa-fw-400" type="text" name="nums-replace_keyword" id="nums-replace_keyword" placeholder=" 取代數量..." />
                  </div>
              </div>
          </div>
          <div class="row m-0 flex-lg-nowrap rpa-gap__10 mb__14" data-form-append="replace_keyword">
              <div class="col-lg-5 col-md-12 p-0 flex-fill">
                  <span class="rpa-fs-14 rpa-text-purple rpa-fw-400"> 特殊連結取代 </span>
                  <div class="input-group mt__4">
                    <input class="form-control shadow-none rpa-input-purple rpa-fs-14 rpa-fw-400" type="text" name="signed_href-replace_keyword" id="signed_href-replace_keyword" placeholder="原本" />
                  </div>
              </div>
              <div class="col-lg-7 col-md-12 p-0 flex-fill">
                  <span class="rpa-fs-14 rpa-text-purple rpa-fw-400"> 取代連結 </span>
                  <div class="input-group mt__4">
                    <input class="form-control shadow-none rpa-input-purple rpa-fs-14 rpa-fw-400" type="text" name="replace_href-replace_keyword" id="replace_href-replace_keyword" placeholder="取代連結" />
                  </div>
              </div>
          </div>`;

// function Random number
const randomNumberGenerator = () => {
  let hash = 0;
  let str = `${new Date().getTime()}`;
  for (let i = 0, len = str.length; i < len; i++) {
    let chr = str.charCodeAt(i);
    hash = (hash << 5) - hash + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return Math.abs(hash);
};

function appendItem(...args) {
  const [formTarget, inpuType, ...combineType] = args;

  const functionalArray = [aHrefTextReplace, aHrefLinkReplace, specialLinkReplace, imagAltReplace];
  const closetInputRow = $(`form[data-form="${formTarget}"] input[data-input="${formTarget}"]`)?.closest(".row");
  const closetRow = $(`form[data-form="${formTarget}"] div[data-form-append="${formTarget}"]`);

  closetInputRow.remove();
  closetRow?.remove();

  if (inpuType !== null && combineType[0] === null) {
    const contentContainer = functionalArray[inpuType].replaceAll("replace_keyword", formTarget);
    $(`form[data-form="${formTarget}"]`)[0].insertAdjacentHTML("beforeend", `${contentContainer}`);
  } else if (inpuType === null && combineType[0] !== null) {
    if (typeof combineType[0] === "string") {
      textInputBox = textInputBox.replaceAll("replace_keyword", formTarget);
      $(`form[data-form="${formTarget}"]`)[0].insertAdjacentHTML("beforeend", `${textInputBox}`);
    } else {
      const wordArray = [$(`input[name="SEO-Subject"]`).val(), $(`input[name="SEO-Adverb"]`).val(), $(`input[name="SEO-Long-Tail-Keyword"]`).val()];
      let combinationText = "";

      // Combine Word
      for (let index = 0; index < wordArray.length; index++) {
        for (let j = 0; j < combineType.length; j++) {
          if (index == combineType[j]) {
            combinationText += wordArray[index];
          }
        }
      }

      let textBoxDisabled = textDisabledInputBox.replaceAll("replace_keyword", formTarget);
      textBoxDisabled = textBoxDisabled.replaceAll("replace_combine_word", combinationText);
      $(`form[data-form="${formTarget}"]`)[0].insertAdjacentHTML("beforeend", `${textBoxDisabled}`);
    }
  } else {
    if (inpuType === 0) {
      if (typeof combineType[0] === "string") {
        let aHrefTextReplaceForm = aHrefTextReplace.replaceAll("replace_keyword", formTarget);
        $(`form[data-form="${formTarget}"]`)[0].insertAdjacentHTML("beforeend", `${aHrefTextReplaceForm}`);
      } else {
        const wordArray = [$(`input[name="SEO-Subject"]`).val(), $(`input[name="SEO-Adverb"]`).val(), $(`input[name="SEO-Long-Tail-Keyword"]`).val()];
        let combinationText = "";

        // Combine Word
        for (let index = 0; index < wordArray.length; index++) {
          for (let j = 0; j < combineType.length; j++) {
            if (index == combineType[j]) {
              combinationText += wordArray[index];
            }
          }
        }

        let aHrefTextReplacWithDisableForm = aHrefTextReplacWithDisable.replaceAll("replace_keyword", formTarget);
        aHrefTextReplacWithDisableForm = aHrefTextReplacWithDisableForm.replaceAll("replace_combine_word", combinationText);
        $(`form[data-form="${formTarget}"]`)[0].insertAdjacentHTML("beforeend", `${aHrefTextReplacWithDisableForm}`);
      }
    }

    if (inpuType === 3) {
      if (typeof combineType[0] === "string") {
        let imagAltReplaceForm = imagAltReplace.replaceAll("replace_keyword", formTarget);
        $(`form[data-form="${formTarget}"]`)[0].insertAdjacentHTML("beforeend", `${imagAltReplaceForm}`);
      } else {
        const wordArray = [$(`input[name="SEO-Subject"]`).val(), $(`input[name="SEO-Adverb"]`).val(), $(`input[name="SEO-Long-Tail-Keyword"]`).val()];
        let combinationText = "";

        // Combine Word
        for (let index = 0; index < wordArray.length; index++) {
          for (let j = 0; j < combineType.length; j++) {
            if (index == combineType[j]) {
              combinationText += wordArray[index];
            }
          }
        }

        let imagAltReplaceWithDisabledForm = imagAltReplaceWithDisabled.replaceAll("replace_keyword", formTarget);
        imagAltReplaceWithDisabledForm = imagAltReplaceWithDisabledForm.replaceAll("replace_combine_word", combinationText);
        $(`form[data-form="${formTarget}"]`)[0].insertAdjacentHTML("beforeend", `${imagAltReplaceWithDisabledForm}`);
      }
    }
  }
}

function deleteRule(formID) {
  $(`div[data-rules-form="${formID}"]`)?.remove();
}

// Show Other Exchange Rules
function showOtherExchangeField(formID = randomNumberGenerator(), targetform = "") {
  const exchangeRulesFormTemplate = exchangeRulesForm.replaceAll("replace_keyword", `${formID}`);
  targetform == ""
    ? document.querySelector('div[data-target="btn-add-env"]').insertAdjacentHTML("beforebegin", `${exchangeRulesFormTemplate}`)
    : document.querySelector(`${targetform} div[data-target="btn-add-env"]`).insertAdjacentHTML("beforebegin", `${exchangeRulesFormTemplate}`);
}
// Save Env Rules Script
const rulesMapping = {
  特殊連結取代: "replace_random_specific_href",
  ahref標籤連結取代: "replace_random_href",
  文字組合一: "replace_file_text",
  文字組合二: "replace_file_text",
  文字組合三: "replace_file_text",
  文字組合四: "replace_file_text",
  ahref標籤文字取代: "replace_assign_text_within_href",
  圖片alt替代: "replace_img_alt",
};

const rulesEnvMapping = { 首頁: 1, 其他: 2 };
const specialLinkMapping = { 全部: 0, 部分: 1 };

function saveEnvDetails(modalID) {
  rpaCloseModal(modalID);

  let rulesEnv = {};
  rulesEnv.scriptName = $(`${modalID}`).find('input[name="script-name"]').val();
  rulesEnv.scriptEditor = $(`${modalID}`).find('textarea[name="script-editor"]').val();
  rulesEnv.seoSubject = $(`${modalID}`).find('input[name="SEO-Subject"]').val();
  rulesEnv.seoAdverb = $(`${modalID}`).find('input[name="SEO-Adverb"]').val();
  rulesEnv.seoLongTailKeyword = $(`${modalID}`).find('input[name="SEO-Long-Tail-Keyword"]').val();

  const dataRulesFormArray = $("div[data-rules-form]");

  let rulesArray = [];

  for (let index = 0; index < dataRulesFormArray.length; index++) {
    const rulesFormID = $(dataRulesFormArray[index]).attr("data-rules-form");
    const formID = dataRulesFormArray[index].querySelector(`[data-form="${rulesFormID}"]`);
    const inputarray = formID.querySelectorAll(`input[type="text"]`);

    let rulesName = $(`#dropdown-rules-${rulesFormID}-span`).text();
    let combineArr = rulesName.includes("(主詞和副詞)")
      ? [0, 1]
      : rulesName.includes("(主詞和長尾詞)")
      ? [0, 2]
      : rulesName.includes("(主詞、副詞、長尾詞)")
      ? [0, 1, 2]
      : rulesName.includes("(純文字組合)")
      ? "blank"
      : null;

    rulesName = rulesName.includes("(主詞和副詞)") ? rulesName.replaceAll("(主詞和副詞)", "") : rulesName;
    rulesName = rulesName.includes("(主詞和長尾詞)") ? rulesName.replaceAll("(主詞和長尾詞)", "") : rulesName;
    rulesName = rulesName.includes("(主詞、副詞、長尾詞)") ? rulesName.replaceAll("(主詞、副詞、長尾詞)", "") : rulesName;
    rulesName = rulesName.includes("(純文字組合)") ? rulesName.replaceAll("(純文字組合)", "") : rulesName;
    rulesName = rulesName.replaceAll(" ", "");
    rulesName = rulesName.trim();

    let rulesKey = {};
    rulesKey.objKey = rulesMapping[`${rulesName}`];
    rulesKey.dropdown = $(`#dropdown-rules-${rulesFormID}-span`).text();
    rulesKey.combination = combineArr;

    for (let j = 0; j < inputarray.length; j++) {
      const inputName = inputarray[j].getAttribute("name").replace(`-${rulesFormID}`, "");
      const inputVal = inputarray[j].value;
      rulesKey[inputName] = inputVal;
    }

    const envRadioValue = $(`input[name="exchange-env-${rulesFormID}"]:checked`).val();
    rulesKey.rulesEnv = rulesEnvMapping[envRadioValue];

    const specialLinkSelectedValue = $(`input[name="special-link-replace-${rulesFormID}"]:checked`)?.val();

    if (specialLinkSelectedValue) {
      rulesKey.specialLink = specialLinkMapping[specialLinkSelectedValue];
    }

    if (rulesKey.objKey) {
      rulesArray.push(rulesKey);
    }
  }

  rulesEnv.rulesArray = rulesArray;

  window.localStorage.setItem($(`${modalID}`).find('input[name="script-name"]').val(), JSON.stringify(rulesEnv));
  let rulesList = JSON.parse(window.localStorage.getItem("ruleslist")) || [];
  let nameAvailable = [];

  rulesList.forEach((item) => {
    nameAvailable.push(item.scriptName);
  });

  let script = $(`${modalID}`).find('input[name="script-name"]').val();
  if (!nameAvailable.includes(script)) {
    rulesList.push({
      scriptName: $(`${modalID}`).find('input[name="script-name"]').val(),
      createBy: "XXX",
      createAt: new Date().getTime(),
      updateBy: new Date().getTime(),
    });
  }

  window.localStorage.setItem("ruleslist", JSON.stringify(rulesList));
  window.location.reload();
}

//   Insert available env rules into dropdown list
let getRulesList = JSON.parse(window.localStorage.getItem("ruleslist")) || [];
if (getRulesList.length !== 0) {
  const envUl = $("#dropdown-05-ul");
  envUl.children().remove();
  let listArray = "";
  getRulesList.forEach((item) => {
    listArray += `<li class="rpa-dropdown-list-item" value="">${item.scriptName}</li>`;
  });
  envUl[0].insertAdjacentHTML("afterbegin", listArray);
}
// Create Web Clone JSON
function exportToJsonFile(jsonData) {
  let dataStr = JSON.stringify(jsonData);
  let dataUri = "data:application/json;charset=utf-8," + encodeURIComponent(dataStr);

  let exportFileDefaultName = `${new Date().getTime()}`;

  let linkElement = document.createElement("a");
  linkElement.setAttribute("href", dataUri);
  linkElement.setAttribute("download", exportFileDefaultName);
  linkElement.click();
}

function createWebCloneMission(modalID) {
  rpaCloseModal(modalID);
  const createForm = $('div[data-form="create-web-clone-form"] input[name]');
  const getArray = { "website-title": "replace_title", "website-description": "replace_description", "website-keyword": "replace_keywords", "website-canonical": "replace_canonical" };
  const redirectScript = "<script type='text/javascript'>" + `setTimeout(() => {window.location.href = "${$('input[name="website-redirect"]').val()}";}, 3000);` + "<" + "/script>";

  const result = {
    main: {
      replace_meta_data: {
        replace_title: "",
        replace_description: "",
        replace_keywords: "",
        replace_canonical: "",
      },
      insert_redirect_script: { redirect_text: redirectScript },
    },
    other: {
      replace_meta_data: {
        replace_title: "",
        replace_description: "",
        replace_keywords: "",
        replace_canonical: "",
      },
      insert_redirect_script: { redirect_text: redirectScript },
    },
  };

  for (let index = 0; index < createForm.length; index++) {
    const element = createForm[index].name;
    const answerKey = getArray[element];
    if (answerKey) {
      result.main.replace_meta_data[`${answerKey}`] = $(`input[name=${element}]`).val();
      result.other.replace_meta_data[`${answerKey}`] = $(`input[name=${element}]`).val();
    }
  }

  const envNameSelected = $(`#dropdown-05-span`).text();
  if (envNameSelected !== "選擇套用SEO情境") {
    const savedEnv = JSON.parse(window.localStorage.getItem(`${envNameSelected}`)) || [];
    const envData = savedEnv.rulesArray;
    envData.forEach((item) => {
      const keyArray = Object.keys(item);
      let eachDataMP = {};
      let eachDataSP = {};

      if (result.main[`${item.objKey}`]?.length == 0 || result.main[`${item.objKey}`] == null) {
        result.main[`${item.objKey}`] = [];
      }
      if (result.other[`${item.objKey}`]?.length == 0 || result.other[`${item.objKey}`] == null) {
        result.other[`${item.objKey}`] = [];
      }

      keyArray.forEach((key) => {
        const skip = ["dropdown", "combination", "rulesEnv", "objKey"];
        if (!skip.includes(key)) {
          if (item["rulesEnv"] == 1) {
            if (key === "specialLink") {
              eachDataMP["type"] = item[`${key}`];
            }
            if (key === "nums") {
              eachDataMP[`${key}`] = parseInt(item[`${key}`]);
            }
            eachDataMP[`${key}`] = item[`${key}`];
          }
          if (item["rulesEnv"] == 2) {
            if (key === "specialLink") {
              eachDataSP["type"] = item[`${key}`];
            }
            if (key === "nums") {
              eachDataSP[`${key}`] = parseInt(item[`${key}`]);
            }
            eachDataSP[`${key}`] = item[`${key}`];
          }
        }
      });

      Object.keys(eachDataMP).length !== 0 ? result.main[`${item.objKey}`].push(eachDataMP) : null;
      Object.keys(eachDataSP).length !== 0 ? result.other[`${item.objKey}`].push(eachDataSP) : null;
    });
  }

  exportToJsonFile(result);
}

// Append env list table into table list
function tableContent() {
  const envTableContent = $("#env-table-content");
  envTableContent.children().remove();
  const rulesListArr = JSON.parse(window.localStorage.getItem("ruleslist")) || [];

  let dataBody = "";
  rulesListArr.forEach((item) => {
    dataBody += `
            <tr scenario_id="" status="">
              <td>${item.scriptName}</td><td>${item.createBy}</td><td>${new Date(item.createAt).toLocaleDateString()} &nbsp; ${new Date(item.createAt).toLocaleTimeString("en-US", {
      hour: "numeric",
      minute: "numeric",
      hour12: true,
    })}</td><td>${new Date(item.updateBy).toUTCString()}</td>
              <td><button class="rpa-btn rpa-btn-sm rpa-btn-pacific-blue text-white" onclick="editEnvModal('${item.scriptName}')">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16"><path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"></path><path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"></path></svg>
                <span class="ml__6">Edit</span></button>
              </td>
            </tr>`;
  });
  envTableContent[0].insertAdjacentHTML("afterbegin", dataBody);

  $("#pages_ul-02").addClass("opacity-0");
}

tableContent();

// Edit env button function
function editEnvModal(scriptID) {
  rpaOpenModal("#edit-env-modal");
  const rulesMapping = {
    replace_random_specific_href: "特殊連結取代",
    replace_random_href: "ahref標籤連結取代",
    replace_file_text: "文字組合一",
    replace_file_text: "文字組合二",
    replace_file_text: "文字組合三",
    replace_file_text: "文字組合四",
    replace_assign_text_within_href: "ahref標籤文字取代",
    replace_img_alt: "圖片alt替代",
  };
  const inputBoxMapping = {
    replace_assign_text_within_href: 0,
    replace_random_href: 1,
    replace_random_specific_href: 2,
    replace_img_alt: 3,
  };

  const { scriptName, scriptEditor, rulesArray, seoSubject, seoAdverb, seoLongTailKeyword } = JSON.parse(window.localStorage.getItem(scriptID));
  const functionalArray = [aHrefTextReplace, aHrefLinkReplace, specialLinkReplace, imagAltReplace];

  $("#edit-env-modal").find('input[name="script-name"]').val(scriptName);
  $("#edit-env-modal").find('textarea[name="script-editor"]').val(scriptEditor);
  $("#edit-env-modal").find('input[name="SEO-Subject"]').val(seoSubject);
  $("#edit-env-modal").find('input[name="SEO-Adverb"]').val(seoAdverb);
  $("#edit-env-modal").find('input[name="SEO-Long-Tail-Keyword"]').val(seoLongTailKeyword);

  rulesArray.forEach((item, index) => {
    const keysArr = Object.keys(item);
    const skipKeys = ["objKey", "dropdown", "combination", "rulesEnv", "specialLink"];
    if (item.objKey) {
      if (index === 0) {
        appendItem("0002", inputBoxMapping[`${item.objKey}`], item.combination?.length > 0 ? item.combination.toString() : item.combination);
        $("#edit-env-modal").find("#dropdown-rules-0002-span").text(item.dropdown);

        if (item.specialLink === 1) {
          $("#edit-env-modal").find('input[name="special-link-replace-0002"]#special-link-replace-part-rules-0002').attr("checked", true);
        } else {
          $("#edit-env-modal").find('input[name="special-link-replace-0002"]#special-link-replace-full-rules-0002').attr("checked", true);
        }

        if (item.rulesEnv === 1) {
          $("#edit-env-modal").find('input[name="exchange-env-0002"]#exchange-env-home-0002').attr("checked", true);
        } else {
          $("#edit-env-modal").find('input[name="exchange-env-0002"]#exchange-env-other-0002').attr("checked", true);
        }

        keysArr.forEach((key) => {
          if (!skipKeys.includes(key)) {
            $("#edit-env-modal").find(`input[name="${key}-0002"]`).val(item[`${key}`]);
          }
        });
      } else {
        const randomNo = randomNumberGenerator();
        showOtherExchangeField(randomNo, "#edit-env-modal");
        appendItem(`${randomNo}`, inputBoxMapping[`${item.objKey}`], item.combination?.length > 0 ? item.combination.toString() : item.combination);
        $("#edit-env-modal").find(`#dropdown-rules-${randomNo}-span`).text(item.dropdown);

        if (item.specialLink === 1) {
          $("#edit-env-modal").find(`input[name="special-link-replace-${randomNo}"]#special-link-replace-part-rules-${randomNo}`).attr("checked", true);
        } else {
          $("#edit-env-modal").find(`input[name="special-link-replace-${randomNo}"]#special-link-replace-full-rules-${randomNo}`).attr("checked", true);
        }

        if (item.rulesEnv === 1) {
          $("#edit-env-modal").find(`input[name="exchange-env-${randomNo}"]#exchange-env-home-${randomNo}`).attr("checked", true);
        } else {
          $("#edit-env-modal").find(`input[name="exchange-env-${randomNo}"]#exchange-env-other-${randomNo}`).attr("checked", true);
        }

        keysArr.forEach((key) => {
          if (!skipKeys.includes(key)) {
            $("#edit-env-modal").find(`input[name="${key}-${randomNo}"]`).val(item[`${key}`]);
          }
        });
      }
    }
  });
}

window.onload = rpaResize();
function rpaResize() {
  if (window.innerWidth <= 601) {
    $(".sidebar.zindex-fixed").addClass("hide");
    $(".main.p__20").addClass("sidebar__hide");
    $(".menu").addClass("sidebar__hide");
  }
}
