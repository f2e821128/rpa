// 沿用既有的。
function checkBoxDropdown() {
  $('[data-toggle="check-all"][check_all_type="search_site"]').change(function () {
    let selectAllStatus = $(this).is(":checked");
    let parentNodes = $(this).parent();
    let inputs = $(parentNodes).find('[name="dropdown-group"]');
    $(inputs).prop("checked", selectAllStatus);
  });

  $('[data-toggle="check-all"][check_all_type="search_tag"]').change(function () {
    let selectAllStatus = $(this).is(":checked");
    let parentNodes = $(this).parent();
    let inputs = $(parentNodes).find('[name="dropdown-group"]');
    $(inputs).prop("checked", selectAllStatus);
  });

  var CheckboxDropdown = function (el) {
    var _this = this;
    this.isOpen = false;
    this.areAllChecked = false;
    this.$el = $(el);
    this.$label = this.$el.find(".rpa-checkbox-dropdown-label");
    let a = this.$el.find('[data-toggle="check-all"]');
    this.$inputs = this.$el.find('[type="checkbox"]');

    this.onCheckBox();

    this.$label.on("click", function (e) {
      e.preventDefault();
      _this.toggleOpen();
    });

    this.$inputs.on("change", function (e) {
      if ($(this).attr("data-toggle") != "check-all") {
        var vchild_num = $(this).parent().parent().parent().parent().find("[name='dropdown-group']").length;
        var vchild_checked_num = $(this).parent().parent().parent().parent().find("[name='dropdown-group']:checked").length;

        if (vchild_num === vchild_checked_num) $(this).parent().parent().parent().parent().find("[name='rpa-checkbox-dropdown-checkall']").prop("checked", true);
        else $(this).parent().parent().parent().parent().find("[name='rpa-checkbox-dropdown-checkall']").prop("checked", false);
      }
      _this.onCheckBox();
    });
  };

  CheckboxDropdown.prototype.onCheckBox = function () {
    this.updateStatus();
  };

  CheckboxDropdown.prototype.updateStatus = function () {
    var checked = this.$el.find("[name='dropdown-group']:checked");
    this.areAllChecked = false;

    if (checked.length <= 0) {
      this.$label.html(this.$label[0]?.getAttribute("lbltitle"));
    } else if (checked.length === this.$inputs.length) {
      this.$label.html("All Selected");
      this.areAllChecked = true;
    } else {
      // TODO：將選項做多語系 num 是已選取的數量。
      this.$label.html(`${this.$label[0].getAttribute("data-selected-text").replace("num", `${checked.length}`)}`);
    }
  };

  CheckboxDropdown.prototype.onCheckAll = function (checkAll) {
    this.updateStatus();
  };

  CheckboxDropdown.prototype.toggleOpen = function (forceOpen) {
    var _this = this;

    if (!this.isOpen || forceOpen) {
      this.isOpen = true;
      this.$el.addClass("on");
      $(document).on("click", function (e) {
        if (!$(e.target).closest("[data-control]").length) {
          _this.toggleOpen();
        }
      });
    } else {
      this.isOpen = false;
      this.$el.removeClass("on");
      $(document).off("click");
    }
  };

  var checkboxesDropdowns = document.querySelectorAll('[data-control="checkbox-dropdown"]');

  for (var i = 0, length = checkboxesDropdowns.length; i < length; i++) {
    new CheckboxDropdown(checkboxesDropdowns[i]);
  }

  let checkboxInputs = document.querySelectorAll(".rpa-checkbox-dropdown-input");
  for (let i = 0; i < checkboxInputs.length; i++) {
    checkboxInputs[i].addEventListener("input", function (e) {
      let searchKeyword = e.target.value;
      let dropdownListParent = checkboxInputs[i].parentNode;
      let topics = dropdownListParent.querySelectorAll('[name="rpa-checkbox-dropdown-checkall"]');

      for (let i = 0; i < topics.length; i++) {
        let topicWrapper = topics[i].parentNode;

        if (topics[i].value.includes(searchKeyword) || ($(topics[i]).attr("text") != undefined && $(topics[i]).attr("text").indexOf(searchKeyword) >= 0)) {
          topicWrapper.classList.remove("hidden");
        } else {
          topicWrapper.classList.add("hidden");
        }
      }
    });
  }

  $(".rpa-checkbox-dropdown-option-topic").unbind();

  $(".rpa-checkbox-dropdown-option-topic").click(function (e) {
    if ($(e.target)?.attr("data-role") === "child") {
      // 不做任何事情
      // console.log("child");
    } else {
      $(this).toggleClass("on");
    }
  });
}
