function sidebarInit() {
  // Sidebar Item Dropdown function.
  $(".sidebar__dropdown > a#rapa_sidebar001").click(function () {
    if ($(this).parent().hasClass("show")) {
      $(this).parent().toggleClass("show");
    } else {
      const sidebarItem = document.querySelectorAll(".sidebar__dropdown");
      sidebarItem.forEach((item) => {
        item.classList.contains("show") ? item.classList.remove("show") : null;
      });
      $(this).parent().toggleClass("show");
    }
  });

  // Hide Sidebar function.
  $("#rpa-sidebar-controller").click(function () {
    $(".sidebar").toggleClass("hide");

    // Move Menu button position & Main Content Size.
    if ($(".menu").hasClass("sidebar__hide")) {
      $(".menu").removeClass("sidebar__hide");
      $(".main").removeClass("sidebar__hide");
    } else {
      $(".main").addClass("sidebar__hide");
      $(".menu").addClass("sidebar__hide");
    }

    const rpa__sidebar__dropdown = document.querySelectorAll(".sidebar__dropdown");

    for (let i = 0; i < rpa__sidebar__dropdown.length; i++) {
      if (rpa__sidebar__dropdown[i].classList.contains("active")) {
        rpa__sidebar__dropdown[i].classList.add("hide__children");
      }
      if (rpa__sidebar__dropdown[i].classList.contains("show")) {
        rpa__sidebar__dropdown[i].classList.remove("show");
      }
    }
  });
}

$(".sidebar__dropdown > a").click(function () {
  if ($(this).parent().hasClass("show")) {
    $(this).parent().toggleClass("show");
  } else {
    const sidebarItem = document.querySelectorAll(".sidebar__dropdown");
    sidebarItem.forEach((item) => {
      item.classList.contains("show") ? item.classList.remove("show") : null;
    });
    $(this).parent().toggleClass("show");
  }
});

// Hide Sidebar function.
$("#rpa-sidebar-controller").click(function () {
  $(".sidebar").toggleClass("hide");

  // Move Menu button position & Main Content Size.
  if ($(".menu").hasClass("sidebar__hide")) {
    $(".menu").removeClass("sidebar__hide");
    $(".main").removeClass("sidebar__hide");
  } else {
    $(".main").addClass("sidebar__hide");
    $(".menu").addClass("sidebar__hide");
  }

  const rpa__sidebar__dropdown = document.querySelectorAll(".sidebar__dropdown");

  for (let i = 0; i < rpa__sidebar__dropdown.length; i++) {
    if (rpa__sidebar__dropdown[i].classList.contains("active")) {
      rpa__sidebar__dropdown[i].classList.add("hide__children");
    }
    if (rpa__sidebar__dropdown[i].classList.contains("show")) {
      rpa__sidebar__dropdown[i].classList.remove("show");
    }
  }
});
