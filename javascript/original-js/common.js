function redirectToPage(pageName, contentid) {
  if (typeof show_type2_timer != "undefined" && show_type2_timer != null) clearInterval(show_type2_timer);
  let path = "view/" + pageName + ".html";
  if (contentid == null || contentid == undefined) {
    $("#main_content_div").empty();
    $("#main_content_div").load(path, function () {});
  } else {
    $("#" + contentid).empty();
    $("#" + contentid).load(path, function () {});
  }
}

function ArrayUnique(value, index, self) {
  return self.indexOf(value) === index;
}

function GetToday() {
  let today = new Date();
  let dd = today.getDate();
  let mm = today.getMonth() + 1;
  let yyyy = today.getFullYear();
  if (dd < 10) {
    dd = "0" + dd;
  }
  if (mm < 10) {
    mm = "0" + mm;
  }
  today = yyyy + "-" + mm + "-" + dd;
  return today;
}

Date.prototype.addDays = function (days) {
  let date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  let month = date.getMonth() + 1 < 10 ? `0${date.getMonth() + 1}` : date.getMonth() + 1;
  let day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
  return `${date.getFullYear()}-${month}-${day}`;
};

function rpaTreeTableInit() {
  document.querySelectorAll('[row-toggle="collapse"]').forEach((item) => {
    item.addEventListener("click", (e) => {
      e.preventDefault();
      if (e.target.tagName.toLowerCase() !== "button") {
        let child = e.target.parentNode;
        if ($(child).next().hasClass("shown")) {
          $(child).next().removeClass("shown");
          $(`i[group="${$($(child).next()[0]).attr("group_code")}"]`).addClass("icon-down");
          $(`i[group="${$($(child).next()[0]).attr("group_code")}"]`).removeClass("icon-up");
        } else {
          $(child).next().addClass("shown");
          $(`i[group="${$($(child).next()[0]).attr("group_code")}"]`).addClass("icon-up");
          $(`i[group="${$($(child).next()[0]).attr("group_code")}"]`).removeClass("icon-down");
        }
      }
    });
  });
}

Array.prototype.remove = function () {
  let what,
    a = arguments,
    L = a.length,
    ax;
  while (L && this.length) {
    what = a[--L];
    while ((ax = this.indexOf(what)) !== -1) {
      this.splice(ax, 1);
    }
  }
  return this;
};

function toCurrency(num) {
  if (!isNaN(num) && num != null && num != undefined) {
    let parts = num.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
  } else return num;
}

function toPercentage(num1, num2, percent = true) {
  if (num1 == 0 || num2 == 0 || num1 == undefined || num2 == undefined) {
    if (percent) return "0%";
    else return 0;
  }

  if (percent) return Math.round((num1 / num2) * 10000) / 100.0 + "%";
  else return Math.round((num1 / num2) * 10000) / 100.0;
}
