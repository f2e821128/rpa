// Dropdown Checkbox BEGIN
(function ($) {
  var RPACheckboxDropdown = function (el) {
    var _this = this;
    this.isOpen = false;
    this.areAllChecked = false;
    this.$el = $(el);
    this.$label = this.$el.find(".rpa__dropdown__label");
    this.$checkAll = this.$el.find('[data-toggle="check-all"]').first();
    this.$inputs = this.$el.find('.rpa__checkbox__input[type="checkbox"]');

    this.$checkedString = this.$el.find(".rpa__dropdown__input");

    const rpaCheckboxChecked = this.$checkedString.prop("value").trim().split(",");

    let checkbox_Input_Array = document.querySelectorAll(".rpa__checkbox__input");

    for (let i = 0; i < checkbox_Input_Array.length; i++) {
      if (rpaCheckboxChecked.includes(checkbox_Input_Array[i].value)) {
        checkbox_Input_Array[i].checked = true;
      }
    }

    this.rpaOnCheckBox();

    this.$label.on("click", function (e) {
      e.preventDefault();
      _this.rpaToggleOpen();
    });

    this.$checkAll.on("click", function (e) {
      e.preventDefault();
      _this.rpaOnCheckAll();
    });

    this.$inputs.on("change", function (e) {
      _this.rpaOnCheckBox();
    });
  };

  RPACheckboxDropdown.prototype.rpaOnCheckBox = function () {
    this.rpaUpdateStatus();
  };

  RPACheckboxDropdown.prototype.rpaUpdateStatus = function () {
    var checked = this.$el.find(":checked");

    this.areAllChecked = false;
    this.$checkAll.html("Check All");

    let inputStr = "";
    for (let i = 0; i < checked.length; i++) {
      inputStr += `${checked[i].value},`;
    }

    this.$checkedString.prop("value", inputStr);

    if (checked.length <= 0) {
      this.$label.html("隢贝身摰𡁏�𢠃��");
    } else if (checked.length === 1) {
      // this.$label.html(checked.parent("label").text());
      this.$label.html("撌脤�� " + checked.length + " 甈𢠃��");
    } else if (checked.length === this.$inputs.length) {
      this.$label.html("�券��");
      this.areAllChecked = true;
      this.$checkAll.html("Uncheck All");
    } else {
      this.$label.html("撌脤�� " + checked.length + " 甈𢠃��");
    }
  };

  RPACheckboxDropdown.prototype.rpaOnCheckAll = function (checkAll) {
    if (!this.areAllChecked || checkAll) {
      this.areAllChecked = true;
      this.$checkAll.html("Uncheck All");
      this.$inputs.prop("checked", true);
    } else {
      this.areAllChecked = false;
      this.$checkAll.html("Check All");
      this.$inputs.prop("checked", false);
    }

    this.rpaUpdateStatus();
  };

  RPACheckboxDropdown.prototype.rpaToggleOpen = function (forceOpen) {
    var _this = this;

    if (!this.isOpen || forceOpen) {
      this.isOpen = true;
      this.$el.addClass("on");

      // When dropdown turn on remove modal label BEGIN
      document.getElementsByClassName("create__user__role__modal__bg")[0].classList.add("modal__dropdown__on");
      // When dropdown turn on remove modal label END

      // When dropdown turn on add dropdown label BEGIN
      document.getElementById("rpa-checkbox-dropdown-bg").classList.add("show");
      // When dropdown turn on add dropdown label END

      $(document).on("click", function (e) {
        if (!$(e.target).closest("[data-control]").length) {
          _this.rpaToggleOpen();
        }
      });
    } else {
      this.isOpen = false;
      this.$el.removeClass("on");

      // When dropdown turn off add modal label BEGIN
      document.getElementsByClassName("create__user__role__modal__bg")[0].classList.remove("modal__dropdown__on");
      // When dropdown turn off add modal label END

      // When dropdown turn on add dropdown label BEGIN
      document.getElementById("rpa-checkbox-dropdown-bg").classList.remove("show");
      // When dropdown turn on add dropdown label END

      $(document).off("click");
    }
  };

  const rpaCheckboxesDropdowns = document.querySelectorAll('[data-control="rpa-Checkbox-Dropdown"]');

  for (var i = 0, length = rpaCheckboxesDropdowns.length; i < length; i++) {
    new RPACheckboxDropdown(rpaCheckboxesDropdowns[i]);
  }
})(jQuery);
// Dropdown Checkbox END

function RoleCheckboxInit(elm) {
  var RPACheckboxDropdown = function (el) {
    var _this = this;
    this.isOpen = false;
    this.areAllChecked = false;
    this.$el = $(el);
    this.$label = this.$el.find(".rpa__dropdown__label");
    this.$checkAll = this.$el.find('[data-toggle="check-all"]').first();
    this.$inputs = this.$el.find('.rpa__checkbox__input[type="checkbox"]');

    this.$checkedString = this.$el.find(".rpa__dropdown__input");

    const rpaCheckboxChecked = this.$checkedString.prop("value").trim().split(",");

    let checkbox_Input_Array = document.querySelectorAll(".rpa__checkbox__input");

    for (let i = 0; i < checkbox_Input_Array.length; i++) {
      if (rpaCheckboxChecked.includes(checkbox_Input_Array[i].value)) {
        checkbox_Input_Array[i].checked = true;
      }
    }

    this.rpaOnCheckBox();

    this.$label.on("click", function (e) {
      e.preventDefault();
      _this.rpaToggleOpen();
    });

    this.$checkAll.on("click", function (e) {
      e.preventDefault();
      _this.rpaOnCheckAll();
    });

    this.$inputs.on("change", function (e) {
      _this.rpaOnCheckBox();
    });
  };

  RPACheckboxDropdown.prototype.rpaOnCheckBox = function () {
    this.rpaUpdateStatus();
  };

  RPACheckboxDropdown.prototype.rpaUpdateStatus = function () {
    var checked = this.$el.find(":checked");

    this.areAllChecked = false;
    this.$checkAll.html("Check All");

    let inputStr = "";
    for (let i = 0; i < checked.length; i++) {
      inputStr += `${checked[i].value},`;
    }
    this.$checkedString.prop("value", inputStr);

    if (checked.length <= 0) {
      this.$label.html("隢贝身摰𡁏�𢠃��");
    } else if (checked.length === 1) {
      // this.$label.html(checked.parent("label").text());
      this.$label.html("撌脤�� " + checked.length + " 甈𢠃��");
    } else if (checked.length === this.$inputs.length) {
      this.$label.html("�券��");
      this.areAllChecked = true;
      this.$checkAll.html("Uncheck All");
    } else {
      this.$label.html("撌脤�� " + checked.length + " 甈𢠃��");
    }
  };

  RPACheckboxDropdown.prototype.rpaOnCheckAll = function (checkAll) {
    if (!this.areAllChecked || checkAll) {
      this.areAllChecked = true;
      this.$checkAll.html("Uncheck All");
      this.$inputs.prop("checked", true);
    } else {
      this.areAllChecked = false;
      this.$checkAll.html("Check All");
      this.$inputs.prop("checked", false);
    }

    this.rpaUpdateStatus();
  };

  RPACheckboxDropdown.prototype.rpaToggleOpen = function (forceOpen) {
    var _this = this;

    if (!this.isOpen || forceOpen) {
      this.isOpen = true;
      this.$el.addClass("on");

      // When dropdown turn on remove modal label BEGIN
      document.getElementsByClassName("create__user__role__modal__bg")[0].classList.add("modal__dropdown__on");
      // When dropdown turn on remove modal label END

      // When dropdown turn on add dropdown label BEGIN
      document.getElementById("rpa-checkbox-dropdown-bg").classList.add("show");
      // When dropdown turn on add dropdown label END

      $(document).on("click", function (e) {
        if (!$(e.target).closest("[data-control]").length) {
          _this.rpaToggleOpen();
        }
      });
    } else {
      this.isOpen = false;
      this.$el.removeClass("on");

      // When dropdown turn off add modal label BEGIN
      document.getElementsByClassName("create__user__role__modal__bg")[0].classList.remove("modal__dropdown__on");
      // When dropdown turn off add modal label END

      // When dropdown turn on add dropdown label BEGIN
      document.getElementById("rpa-checkbox-dropdown-bg").classList.remove("show");
      // When dropdown turn on add dropdown label END

      $(document).off("click");
    }
  };

  new RPACheckboxDropdown(elm);
}
