// Dropdown Javascript BEGIN
function dropdownOnInit() {
  let linkToggle = document.querySelectorAll(".custom-select");

  //   let dropdownBg = document.getElementById("dropdown__background-unique");

  for (let i = 0; i < linkToggle.length; i++) {
    linkToggle[i].addEventListener("click", function (event) {
      event.preventDefault();
      var container = document.getElementById(this.dataset.container);

      //   dropdownBg.classList.add("active");

      var dataset = document.querySelectorAll("div[data-container]");
      for (let j = 0; j < dataset.length; j++) {
        var options = document.getElementById(dataset[j].dataset.container);
        if (options.id !== container.id) {
          options.classList.remove("active");
        }
      }

      if (!container.classList.contains("active")) {
        this.classList.add("active");
        container.classList.add("active");
      } else {
        this.classList.remove("active");
        container.classList.remove("active");
      }
    });
  }

  // lister which user choose.
  for (const option of document.querySelectorAll(".select-options li")) {
    option.addEventListener("click", function () {
      this.parentNode.parentNode.querySelector(".custom-select span").textContent = this.textContent;
    });
  }

  window.onclick = function (event) {
    let linkToggles = document.querySelector(".custom-select");
    let modalMultiSelect = document.querySelector(".multi-select-options");
    let dropdownBgLabel = document.getElementById("dropdown__background-unique");

    const createModalLabel = document.getElementsByClassName("create__user__modal__bg");

    if (linkToggles && linkToggles.classList.contains("active")) {
      if (!event.target.matches(".option-selected")) {
        let dropdowns = document.getElementsByClassName("select-options");

        // Remove child active status.
        for (let i = 0; i < linkToggle.length; i++) {
          let openDropdown = dropdowns[i];
          if (openDropdown.classList.contains("active")) {
            openDropdown.classList.remove("active");
          }
        }

        // Remove active when user click outside
        for (let i = 0; i < linkToggle.length; i++) {
          linkToggle[i].classList.remove("active");
        }
      }
    }

    if (modalMultiSelect && modalMultiSelect.classList.contains("active")) {
      if (event.target.matches(".single__input")) {
        modalMultiSelect.classList.remove("active");
        dropdownBgLabel.classList.remove("active");
        createModalLabel[0] ? (createModalLabel[0].style.display = "") : null;
      }

      if (!event.target.matches("input[type=text]")) {
        modalMultiSelect.classList.remove("active");
        dropdownBgLabel.classList.remove("active");
        createModalLabel[0] ? (createModalLabel[0].style.display = "") : null;
      }

      if (event.target.matches(".dropdown__background")) {
        modalMultiSelect.classList.remove("active");
        dropdownBgLabel.classList.remove("active");
        createModalLabel[0] ? (createModalLabel[0].style.display = "") : null;
      }
    }
  };
}

// Dropdown Javascript END
// var dropdownTags = new MultiSelectTags("#dropdown-value");
var dropdownTags_1 = new MultiSelectTags("#dropdown-value-1");
// var dropdownTags_2 = new MultiSelectTags("#dropdown-value-2");

// var dropdownTags_3 = new MultiSelectTags("#dropdown-value-3");

function multiSelectOnInit() {
  const multiSelectToggle = document.querySelectorAll(".custom-select-with-tags");

  for (let i = 0; i < multiSelectToggle.length; i++) {
    multiSelectToggle[i].addEventListener("click", function (event) {
      event.preventDefault();
      const container = document.getElementById(this.dataset.container);
      const dropdownBgLabel = document.getElementById("dropdown__background-unique");
      const createModalLabel = document.getElementsByClassName("create__user__modal__bg");

      const dataset = document.querySelectorAll("div[data-container]");
      for (let j = 0; j < dataset.length; j++) {
        const options = document.getElementById(dataset[j].dataset.container);
        if (options.id !== container.id) {
          options.classList.remove("active");
          dropdownBgLabel.classList.remove("active");
          createModalLabel[0].style.display = "block";
        }
      }

      if (!container.classList.contains("active")) {
        container.classList.add("active");
        dropdownBgLabel.classList.add("active");
        createModalLabel[0].style.display = "none";
      } else {
        container.classList.remove("active");
        dropdownBgLabel.classList.remove("active");
        createModalLabel[0].style.display = "";
      }
    });
  }
}

function dropdownTagsClick(el, dt) {
  let rpaRoleSelectValue = el.getAttribute("data");
  // dropdownTags.addTags(rpaRoleSelectValue);
  let rpaMultiSelectTags = document.getElementById("multiselect-with-tags-" + dt);

  if (dt == 1) {
    dropdownTags_1.addTags(rpaRoleSelectValue);
  }
  // else if (dt == 2) {
  //     dropdownTags_2.addTags(rpaRoleSelectValue);
  // }
  // else if(dt==3){
  //     dropdownTags_3.addTags(rpaRoleSelectValue);
  // }

  let rpaListItem = rpaMultiSelectTags.getElementsByTagName("li");

  for (i = 0; i < rpaListItem.length; i++) {
    txtValue = rpaListItem[i].textContent || rpaListItem[i].innerText;
    rpaListItem[i].style.display = "";
  }
}

// Dropdown Javascript END

$("#rpa-dropdown-label-input").on("input", function () {
  let input, filter, a, i;
  input = document.getElementById("rpa-dropdown-label-input");
  filter = input.value.toUpperCase();
  div = document.getElementById("multiselect-with-tags-1");
  a = div.getElementsByTagName("li");

  for (i = 0; i < a.length; i++) {
    txtValue = a[i].textContent || a[i].innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      a[i].style.display = "";
    } else {
      a[i].style.display = "none";
    }
  }
});

// Functional Dropdown BEGIN
function rpaDropdownFunc(key) {
  let selectDropdown = document.getElementById(key);
  let selectDropdownParent = selectDropdown.parentNode;
  let selectDropdownList = selectDropdownParent.querySelectorAll(`[data-container="${key}"]`)[0];

  if (selectDropdown.hasAttribute("disabled")) {
    return;
  }

  let selectDropdownChild = selectDropdownList.children;
  let selectDropdownSpan = selectDropdown.children;
  for (let i = 0; i < selectDropdownChild.length; i++) {
    selectDropdownChild[i].addEventListener("click", function () {
      selectDropdownSpan[0].textContent = selectDropdownChild[i].textContent;
      selectDropdown.classList.remove("active");
      selectDropdownList.classList.remove("active");
    });
  }

  if (!selectDropdown.classList.contains("active")) {
    selectDropdown.classList.add("active");
    selectDropdownList.classList.add("active");
  } else {
    closeDropdownFunc(key);
  }
}

function closeDropdownFunc(key) {
  if (key) {
    let selectDropdown = document.getElementById(key);
    let selectDropdownParent = selectDropdown.parentNode;
    let selectDropdownList = selectDropdownParent.querySelectorAll(`[data-container="${key}"]`)[0];

    selectDropdown.classList.remove("active");
    selectDropdownList.classList.remove("active");
  } else {
    let allDropdown = document.querySelectorAll(".custom__dropdown");
    for (let i = 0; i < allDropdown.length; i++) {
      let dropdownSpan = allDropdown[i].children[0];
      let dropdownList = allDropdown[i].children[1];

      dropdownSpan.classList.contains("active") ? dropdownSpan.classList.remove("active") : null;

      dropdownList.classList.contains("active") ? dropdownList.classList.remove("active") : null;
    }
  }
}

// Listen to the doc click
window.addEventListener("click", function (e) {
  // Close the menu if click happen outside menu
  if (e.target.closest(".custom__dropdown") === null) {
    // Close the opend dropdown
    closeDropdownFunc();
  }
  // else {
  //   closeDropdownFunc();
  //   rpaDropdownFunc(
  //     e.target.closest(".custom__dropdown-selected")?.getAttribute("id")
  //   );
  // }
});

// Functional Dropdown END

function inputBoxInit(img_url, target_link) {
  const tripleInputBox = document.createElement("div");
  tripleInputBox.className = "row__triple__input row__triple__input_type2 addnewimg";

  const inputBoxTitle = ["delete", "��𣇉������", "蝬脤�����", "addnew"];

  inputBoxTitle.forEach((title) => {
    const inputBox = document.createElement("div");
    inputBox.className = "input__box";
    if (title == "delete") {
      inputBox.className = "input__box del_div";
      inputBox.style.flex = "0.5";
      inputBox.style.cursor = "pointer";
      const delicon = document.createElement("li");
      delicon.className = "icon-red-trash-12";
      delicon.onclick = function () {
        this.parentNode.parentNode.remove();
        var plus_divs = document.getElementsByClassName("plus_div");
        if (plus_divs != null && plus_divs != undefined && plus_divs.length > 0) plus_divs[plus_divs.length - 1].style.visibility = "visible";

        PreviewShowType2Handle();
        CheckImgItme();
      };
      inputBox.appendChild(delicon);
    } else if (title == "��𣇉������") {
      const spanTitle = document.createElement("span");
      spanTitle.className = "input__box-title";
      spanTitle.textContent = `${title}`;

      const tooltip_a = document.createElement("a");
      tooltip_a.href = "#";
      tooltip_a.className = "rpa__tooltip";
      tooltip_a.style.marginLeft = "10px";
      tooltip_a.setAttribute("data-tooltip", "��鞟內: ��𣇉��祝擃� 300px * 300px");
      const tooltip_li = document.createElement("li");
      tooltip_li.className = "icon-information-14";
      tooltip_li.style.display = "inline-block";

      tooltip_a.appendChild(tooltip_li);
      spanTitle.appendChild(tooltip_a);

      const inputText = document.createElement("input");
      inputText.className = "input__box-wrapper";
      inputText.setAttribute("type", "text");
      inputText.name = "imgurl";
      inputText.placeholder = "摮埈彍��𣂼�:500";
      inputText.maxLength = 500;
      inputText.onblur = function () {
        PreviewShowType2Handle();
      };
      inputText.onkeyup = function () {
        PreviewShowType2Handle();
      };

      inputBox.appendChild(spanTitle);
      inputBox.appendChild(inputText);
    } else if (title == "addnew") {
      inputBox.className = "input__box plus_div";
      inputBox.style.flex = "0.5";
      inputBox.style.cursor = "pointer";
      inputBox.style.padding = "0";
      inputBox.name = "plus";

      const plusicon = document.createElement("li");
      plusicon.className = "icon-plus-circle-blue";
      plusicon.onclick = function () {
        this.parentNode.style.visibility = "hidden";

        createMultiInput();
        PreviewShowType2Handle();
      };
      inputBox.appendChild(plusicon);
    } else {
      // 蝬脤�����
      const spanTitle = document.createElement("span");
      spanTitle.className = "input__box-title";
      spanTitle.textContent = `${title}`;

      const inputText = document.createElement("input");
      inputText.className = "input__box-wrapper";
      inputText.setAttribute("type", "text");
      inputText.name = "pagelink";
      inputText.placeholder = "摮埈彍��𣂼�:300";
      inputText.maxLength = 300;

      inputBox.appendChild(spanTitle);
      inputBox.appendChild(inputText);
    }

    tripleInputBox.appendChild(inputBox);
  });
  if (img_url != null && target_link != null) {
    var txts = tripleInputBox.getElementsByClassName("input__box-wrapper");
    if (txts != null && txts.length == 2) {
      txts[0].value = img_url;
      txts[1].value = target_link;
    }
  }
  CheckImgItme();
  return tripleInputBox;
}

function CheckImgItme() {
  $(".plus_div").css("visibility", "hidden");
  var plus_divs = $(".plus_div");
  if (plus_divs.length < 3) {
    $($(".plus_div")[plus_divs.length - 1]).css("visibility", "visible");
  }
}

var type3_btn_count = 1;
function inputType_3_Init(btn_type, btn_text, btn_link) {
  const tripleInputBox = document.createElement("div");
  tripleInputBox.className = "row__triple__input addnewbtn row__triple__input_type3";

  const inputBoxTitle = ["addnew", "delete", "��厰�閖�𧼮��", "��厰�閙����", "蝬脤�����"];

  inputBoxTitle.forEach((title) => {
    const inputBox = document.createElement("div");
    inputBox.className = "input__box";
    if (title == "addnew") {
      inputBox.className = "input__box plus_btn_div";
      inputBox.name = "plus_btn";
      inputBox.style.width = "6%";

      const plusicon = document.createElement("li");
      plusicon.className = "icon-plus-circle-blue";
      plusicon.onclick = function () {
        this.parentNode.style.visibility = "hidden";

        CreateNewBtn();
      };
      inputBox.appendChild(plusicon);
      var plus_btn_divs = document.getElementsByClassName("plus_btn_div");
      if (plus_btn_divs != null && plus_btn_divs != undefined && plus_btn_divs.length > 0 && plus_btn_divs.length == 2) inputBox.style.visibility = "hidden";
    } else if (title == "delete") {
      inputBox.className = "input__box del_btn_div";
      inputBox.style.width = "6%";
      // inputBox.style.cursor = "pointer";
      const delicon = document.createElement("li");
      delicon.className = "icon-red-trash-12";
      delicon.onclick = function () {
        this.parentNode.parentNode.remove();
        var plus_btn_divs = document.getElementsByClassName("plus_btn_div");
        if (plus_btn_divs != null && plus_btn_divs != undefined && plus_btn_divs.length > 0 && plus_btn_divs.length < 3) plus_btn_divs[plus_btn_divs.length - 1].style.visibility = "visible";
        PreviewShowType3Handle();
      };
      inputBox.appendChild(delicon);
    } else if (title == "��厰�閖�𧼮��") {
      inputBox.style.width = "fit-content";
      inputBox.style.flex = "unset";
      inputBox.style.marginRight = "0";
      inputBox.style.paddingRight = "0";

      const spanTitle = document.createElement("span");
      spanTitle.className = "input__box-title";
      spanTitle.style.marginRight = "0";
      spanTitle.textContent = `${title}`;

      const radio_div = document.createElement("div");
      radio_div.className = "single__layer-radio__button";

      const radio_label_1 = document.createElement("label");
      radio_label_1.textContent = "����";
      radio_label_1.className = "media-type__item";
      radio_label_1.style.marginRight = "0";

      const radio1 = document.createElement("input");
      radio1.setAttribute("type", "radio");
      radio1.setAttribute("checked", true);
      radio1.value = "0";
      radio1.name = `btntype_radio_${type3_btn_count}`;
      radio1.onclick = function () {
        this.parentNode.parentNode.parentNode.parentNode.childNodes[4].style.visibility = "visible";
      };
      radio_label_1.appendChild(radio1);

      const radio_span1 = document.createElement("span");
      radio_span1.className = "radio__mark";
      radio_label_1.appendChild(radio_span1);

      const radio_label_2 = document.createElement("label");
      radio_label_2.textContent = "��𣈯��";
      radio_label_2.className = "media-type__item";

      const radio2 = document.createElement("input");
      radio2.setAttribute("type", "radio");
      radio2.value = "1";
      radio2.name = `btntype_radio_${type3_btn_count}`;
      radio2.onclick = function () {
        this.parentNode.parentNode.parentNode.parentNode.childNodes[4].style.visibility = "hidden";
        this.parentNode.parentNode.parentNode.parentNode.childNodes[4].value = "";
      };

      const radio_span2 = document.createElement("span");
      radio_span2.className = "radio__mark";
      radio_label_2.appendChild(radio2);
      radio_label_2.appendChild(radio_span2);

      radio_div.appendChild(radio_label_1);
      radio_div.appendChild(radio_label_2);

      inputBox.appendChild(spanTitle);
      inputBox.appendChild(radio_div);
    } else {
      inputBox.style.width = "fit-content";

      const inputText = document.createElement("input");
      inputText.className = "input__box-wrapper type3_btn_text";
      inputText.setAttribute("type", "text");

      if (title == "��厰�閙����") {
        //��厰�閙����
        inputText.style.width = "100px";
        inputBox.style.flex = "unset";
        inputText.placeholder = "��厰�閙����";
        inputText.title = "摮埈彍��𣂼�:50";
        inputText.maxLength = 50;
        inputText.onblur = function () {
          PreviewShowType3Handle();
        };
        inputText.onkeyup = function () {
          PreviewShowType3Handle();
        };
        inputText.name = "btn_text";
      } else {
        //蝬脤�����
        inputText.name = "btn_link";
        inputText.placeholder = "蝬脤�����(摮埈彍��𣂼�:300)";
        inputText.maxLength = 300;
      }

      // inputBox.appendChild(spanTitle);
      inputBox.appendChild(inputText);
    }

    tripleInputBox.appendChild(inputBox);
  });
  if (btn_type != null && btn_text != null && btn_link != null) {
    var txts = tripleInputBox.getElementsByClassName("type3_btn_text");
    if (txts != null && txts.length == 2) {
      txts[0].value = btn_text;
      txts[1].value = btn_link;
    }
    //蝺刻摩����厰�閖�𧼮�钅��瘝鍦神
  }
  type3_btn_count++;
  return tripleInputBox;
}

function createMultiInput(img_url, target_link, img_alt) {
  const multiInputWrapper = document.getElementById("show_type_2_img_block");

  multiInputWrapper.appendChild(inputBoxInit(img_url, target_link));

  const imgaltBox = document.createElement("div");
  imgaltBox.className = "row__triple__input row__triple__input_type2_2 addnewimg";
  imgaltBox.style.paddingLeft = "40px";
  const inputBox = document.createElement("div");
  inputBox.className = "input__box";

  const spanTitle = document.createElement("span");
  spanTitle.className = "input__box-title";
  spanTitle.textContent = `��𣇉�䰲LT`;

  const inputText = document.createElement("input");
  inputText.className = "input__box-wrapper";
  inputText.setAttribute("type", "text");
  inputText.style.flex = "unset";
  inputText.name = "imgalt";
  inputText.placeholder = "摮埈彍��𣂼�:100";
  inputText.maxLength = 100;
  if (img_alt != null && img_alt != undefined) inputText.value = img_alt;

  inputBox.appendChild(spanTitle);
  inputBox.appendChild(inputText);

  imgaltBox.appendChild(inputBox);

  multiInputWrapper.appendChild(imgaltBox);

  CheckImgItme();
}

function CreateNewBtn(btn_type, btn_text, btn_link) {
  const multiInputWrapper = document.getElementById("type_3_setting");

  multiInputWrapper.appendChild(inputType_3_Init(btn_type, btn_text, btn_link));
  PreviewShowType3Handle();
}

function PreviewShowType1Handle() {
  $("#preview_show_type1_title").text($("#title_txt").val() != "" ? $("#title_txt").val() : "璅䠷��");
  $("#preview_show_type1_content").text($("#content_txt").val() != "" ? $("#content_txt").val() : "�批捆");

  $("#preview_show_type1_img").attr("src", $("#img_url_txt").val() != "" ? $("#img_url_txt").val() : "./images/rpa05.png");
  $("#preview_show_type1_notify").text($("#notify_text_txt").val() != "" ? $("#notify_text_txt").val() : "�𡁶䰻甈������");
  $("#preview_show_type1_gotext").text($("#goto_text_txt").val() != "" ? $("#goto_text_txt").val() : "��滚���厰�閙����");
  $("#preview_show_type1_closetext").text($("#close_text_txt").val() != "" ? $("#close_text_txt").val() : "��𣈯�㗇�厰�閙����");
}

var show_type2_timer = null;
function PreviewShowType2Handle() {
  $(".addnewpreviewimg").remove();
  $(".firstpreviewimg").show();
  $('[name="preview_show_type2_img"]').attr("src", $("#img_url_0_txt").val() != "" ? $("#img_url_0_txt").val() : "./images/rpa05.png");
  var imgurlary = $('[name="imgurl"]');
  for (var i = 1; i < imgurlary.length; i++) {
    var htmlstr = "";
    htmlstr += `<div class="rpa-ads-slides__wrapper addnewpreviewimg previewimg" style="display: none;">`;
    htmlstr += `    <a href="#" >`;
    htmlstr += `    <img src="${imgurlary[i].value != "" ? imgurlary[i].value : "./images/rpa05.png"}" alt="" class="imgset"  name="preview_show_type2_img" /></a>`;
    htmlstr += `</div>`;
    $(".preview").append(htmlstr);
  }

  let previewindex = 0;
  let slides = document.querySelectorAll(".previewimg");

  // let dot = document.querySelectorAll(".rpa-ads-bottom__left .rpa-ads-image-dot");
  function changeImg() {
    if (previewindex < 0) {
      previewindex = slides.length - 1;
    }

    if (previewindex > slides.length - 1) {
      previewindex = 0;
    }

    for (let i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
      // dot[i].classList.remove("rpa-ads-image-dot-active");
    }

    slides[previewindex].style.display = "block";
    // dot[previewindex].classList.add("rpa-ads-image-dot-active");

    previewindex++;

    // setTimeout(changeImg, 5000);
  }
  if (show_type2_timer != null) clearInterval(show_type2_timer);
  show_type2_timer = setInterval(function () {
    changeImg();
  }, 5000);
}

function PreviewShowType3Handle() {
  var vshow_type_3_default_content =
    ' <p style="text-align: center">' + '   <img src="./images/rpa05.png" alt="" style="width: 182px" class="rpa_img">' + "</p>" + '<p style=" text-align: center; font-size: 25px; ">����堒�批捆</p>';
  var content = CKEDITOR.instances.content_ckeditor.getData() != "" ? CKEDITOR.instances.content_ckeditor.getData() : vshow_type_3_default_content;
  $(".show_type_div").hide();
  var btncount = document.getElementsByClassName("row__triple__input_type3").length;
  if (btncount == 1) {
    $("#show_type_3_1").show();
    $("#show_type_3_1_btn_1").text($($('[name="btn_text"]')[0]).val().trim() == "" ? "��厰�蓥�" : $($('[name="btn_text"]')[0]).val().trim());
    $("#show_type_3_1_content").empty();
    $("#show_type_3_1_content").append(content);
  } else if (btncount == 2) {
    $("#show_type_3_2").show();
    $("#show_type_3_2_btn_1").text($($('[name="btn_text"]')[0]).val().trim() == "" ? "��厰�蓥�" : $($('[name="btn_text"]')[0]).val().trim());
    $("#show_type_3_2_btn_2").text($($('[name="btn_text"]')[1]).val().trim() == "" ? "��厰�蓥��" : $($('[name="btn_text"]')[1]).val().trim());
    $("#show_type_3_2_content").empty();
    $("#show_type_3_2_content").append(content);
  } else if (btncount == 3) {
    $("#show_type_3_3").show();
    $("#show_type_3_3_btn_1").text($($('[name="btn_text"]')[0]).val().trim() == "" ? "��厰�蓥�" : $($('[name="btn_text"]')[0]).val().trim());
    $("#show_type_3_3_btn_2").text($($('[name="btn_text"]')[1]).val().trim() == "" ? "��厰�蓥��" : $($('[name="btn_text"]')[1]).val().trim());
    $("#show_type_3_3_btn_3").text($($('[name="btn_text"]')[2]).val().trim() == "" ? "��厰�蓥��" : $($('[name="btn_text"]')[2]).val().trim());
    $("#show_type_3_3_content").empty();
    $("#show_type_3_3_content").append(content);
  }
}

function openModal(target) {
  let modalInput = document.getElementById(target);
  modalInput.checked = true;
}

function closeModal(target) {
  let modalInput = document.getElementById(target);
  modalInput.checked = false;
}
