// loading structure Init
function loadingInit() {
  let wrapDiv = document.createElement("div");
  let loadingDiv = document.createElement("div");
  let custom = document.createElement("div");
  let customload = document.createElement("span");

  wrapDiv.className = "rpa__loading__wrapper";
  loadingDiv.className = "rpa__loading__load";
  custom.className = "spinner-border rpa-spinner-border rpa-text-purple rpa-w-100 rpa-h-100 fw-bold";
  custom.setAttribute("role", "status");
  customload.className = "visually-hidden";
  customload.textContent = "Loading...";

  custom.appendChild(customload);
  loadingDiv.appendChild(custom);
  wrapDiv.appendChild(loadingDiv);

  return wrapDiv;
}

// function to show loading.
function showLoading(contentArea, showStatus, positionStatus = false) {
  if (showStatus) {
    var noloading = true;
    let container = document.getElementById(`${contentArea}`);
    let childNodes = container.childNodes;
    for (var i = 0; i < childNodes.length; i++) {
      if (childNodes[i].classList != undefined && childNodes[i].classList.contains("rpa__loading__wrapper")) {
        noloading = false;
        break;
      }
    }
    if (noloading) {
      let loadEffect = loadingInit();
      let child = container.firstElementChild;

      container.style.position = positionStatus ? "relative" : "";
      container.classList.add("overflow-hidden");
      loadEffect.style.top = `${window.pageYOffset}px`;
      container.insertBefore(loadEffect, child);
    }
  } else {
    let container = document.getElementById(`${contentArea}`);

    container.style.position = "";
    container.classList.remove("overflow-hidden");
    let childNodes = container.childNodes;
    for (var i = 0; i < childNodes.length; i++) {
      if (childNodes[i].classList != undefined && childNodes[i].classList.contains("rpa__loading__wrapper")) {
        container.removeChild(childNodes[i]);
      }
    }
  }
}

// open loading effect at sidebar
function opensidebarLoading() {
  showLoading("telegram-group-bot-rpa-sidebar", true);
}

// close loading effect at sidebar
function closesidebarLoading() {
  showLoading("telegram-group-bot-rpa-sidebar", false);
}

// open loading effect at main content
function openRobotListLoading() {
  showLoading("telegram-group-bot-rpa-robotlist", true, true);
}

// close loading effect at main content
function closeRobotListLoading() {
  showLoading("telegram-group-bot-rpa-robotlist", false);
}

function openmainblockLoading(id) {
  showLoading(id, true, true);
}

function openblockLoading(id) {
  showLoading(id, true);
}

function closeblockLoading(id) {
  showLoading(id, false);
}
