function checkBoxDropdown() {
  var CheckboxDropdown = function (el) {
    var _this = this;
    this.isOpen = false;
    this.areAllChecked = false;
    this.$el = $(el);
    this.$label = this.$el.find(".rpa-checkbox__dropdown-label");
    let a = this.$el.find('[data-toggle="check-all"]');
    this.$inputs = this.$el.find('[type="checkbox"]');

    this.onCheckBox();

    this.$label.on("click", function (e) {
      e.preventDefault();
      _this.toggleOpen();
    });

    $('[data-toggle="check-all"]').change(function () {
      let selectAllStatus = $(this).is(":checked");
      let parentNodes = $(this).parent().parent();
      let inputs = $(parentNodes).find('[name="dropdown-group"]');
      selectAllStatus ? $(inputs).prop("checked", true) : $(inputs).prop("checked", false);
    });

    this.$inputs.on("change", function (e) {
      _this.onCheckBox();
    });
  };

  CheckboxDropdown.prototype.onCheckBox = function () {
    this.updateStatus();
  };

  CheckboxDropdown.prototype.updateStatus = function () {
    var checked = this.$el.find(":checked");

    this.areAllChecked = false;

    if (checked.length <= 0) {
      this.$label.html(this.$label.attr("data-text"));
    }
    // else if (checked.length === 1) {
    //   this.$label.html(checked.parent("label").text());
    // }
    else if (checked.length === this.$inputs.length) {
      this.$label.html("All Selected");
      this.areAllChecked = true;
    } else {
      this.$label.html(checked.length + " Selected");
    }
  };

  CheckboxDropdown.prototype.onCheckAll = function (checkAll) {
    // console.log("checkAll");

    this.updateStatus();
  };

  CheckboxDropdown.prototype.toggleOpen = function (forceOpen) {
    var _this = this;

    if (!this.isOpen || forceOpen) {
      this.isOpen = true;
      this.$el.addClass("on");
      $(document).on("click", function (e) {
        if (!$(e.target).closest("[data-control]").length) {
          _this.toggleOpen();
        }
      });
    } else {
      this.isOpen = false;
      this.$el.removeClass("on");
      $(document).off("click");
    }
  };

  var checkboxesDropdowns = document.querySelectorAll('[data-control="checkbox-dropdown"]');

  for (var i = 0, length = checkboxesDropdowns.length; i < length; i++) {
    new CheckboxDropdown(checkboxesDropdowns[i]);
  }

  let checkboxInputs = document.querySelectorAll(".rpa-checkbox__dropdown-input");

  for (let i = 0; i < checkboxInputs.length; i++) {
    checkboxInputs[i].addEventListener("input", function (e) {
      let searchKeyword = e.target.value;
      let dropdownListParent = checkboxInputs[i].parentNode;
      let topics = dropdownListParent.querySelectorAll('[name="rpa-checkbox__dropdown-checkall"]');

      for (let i = 0; i < topics.length; i++) {
        let topicWrapper = topics[i].parentNode.parentNode;

        if (topics[i].value.includes(searchKeyword)) {
          topicWrapper.classList.remove("hidden");
        } else {
          topicWrapper.classList.add("hidden");
        }
      }
    });
  }
}
