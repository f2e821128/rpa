function rpaToastInit() {
    rpaToastDestroy();
  const TOASTCONTAINER = `<div class="toast-container position-fixed top-0 end-0 p-3">
    <!-- Toast Example Primary -->
    <div id="primary-toast" class="toast align-items-center rpa-bg-purple text-white border-0" role="alert" aria-live="assertive" aria-atomic="true">
      <div class="d-flex">
        <div class="toast-body">Hello, world! This is a PRIMARY toast message.</div>
        <button type="button" class="btn-close btn-close-white me-2 m-auto fw-bold rpa-fs-20" data-bs-dismiss="toast" aria-label="Close"></button>
      </div>
    </div>
    <!-- Toast Example Warning -->
    <div id="warning-toast" class="toast align-items-center rpa-bg-yellow text-white border-0" role="alert" aria-live="assertive" aria-atomic="true">
      <div class="d-flex">
        <div class="toast-body">Hello, world! This is a WARNING toast message.</div>
        <button type="button" class="btn-close btn-close-white me-2 m-auto fw-bold rpa-fs-20" data-bs-dismiss="toast" aria-label="Close"></button>
      </div>
    </div>
    <!-- Toast Example Danger -->
    <div id="danger-toast" class="toast align-items-center rpa-bg-red text-white border-0" role="alert" aria-live="assertive" aria-atomic="true">
      <div class="d-flex">
        <div class="toast-body">Hello, world! This is a DANGER toast message.</div>
        <button type="button" class="btn-close btn-close-white me-2 m-auto fw-bold rpa-fs-20" data-bs-dismiss="toast" aria-label="Close"></button>
      </div>
    </div>
    <!-- Toast Example Info -->
    <div id="info-toast" class="toast align-items-center rpa-bg-neon_blue text-white border-0" role="alert" aria-live="assertive" aria-atomic="true">
      <div class="d-flex">
        <div class="toast-body">Hello, world! This is a INFO toast message.</div>
        <button type="button" class="btn-close btn-close-white me-2 m-auto fw-bold rpa-fs-20" data-bs-dismiss="toast" aria-label="Close"></button>
      </div>
    </div>
    <!-- Toast Example Success -->
    <div id="success-toast" class="toast align-items-center rpa-bg-brand-success text-white border-0" role="alert" aria-live="assertive" aria-atomic="true">
      <div class="d-flex">
        <div class="toast-body">Hello, world! This is a SUCCESS toast message.</div>
        <button type="button" class="btn-close btn-close-white me-2 m-auto fw-bold rpa-fs-20" data-bs-dismiss="toast" aria-label="Close"></button>
      </div>
    </div>
  </div>`;

  $("body > .container-fluid.p-0").append(TOASTCONTAINER);
}

function rpaToastDestroy() {
  $(".toast-container.position-fixed.top-0.end-0.p-3").remove();
}

function rpaToastShowByID(toastID, content) {
  rpaToastInit();
  const selectToast = $(`#${toastID}`);
  const selectToastBody = $(`#${toastID} .toast-body`);
  const toastStyle = new bootstrap.Toast(selectToast);
  selectToastBody.html(content);
  toastStyle.show();
  selectToast.on('hidden.bs.toast', () => {
    rpaToastDestroy();
  })
}
