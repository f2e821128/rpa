// 當使用多個 Multi Select Tag 時就要初始化多個
let dropdownTags1 = new MultiSelectTags("#rpa-multiselect-value-1");
let dropdownTags2, dropdownTags3, dropdownTags4;

if (window.location.href.split("/").at(-1) != "select2.html") {
  dropdownTags2 = new MultiSelectTags("#rpa-multiselect-value-2");
  dropdownTags3 = new MultiSelectTags("#rpa-multiselect-value-3");
  let dropdownTags4 = new MultiSelectTags("#rpa-multiselect-value-4");
}

function rpaMultiSelectInit() {
  const multiSelectToggle = document.querySelectorAll(".rpa-select-with-tags");

  for (let i = 0; i < multiSelectToggle.length; i++) {
    multiSelectToggle[i].addEventListener("click", function (event) {
      event.preventDefault();
      const container = document.getElementById(this.dataset.container);

      const dataset = document.querySelectorAll("div[data-container]");
      for (let j = 0; j < dataset.length; j++) {
        const options = document.getElementById(dataset[j].dataset.container);
        if (options.id !== container.id) {
          options.classList.remove("active");
          options.previousElementSibling.classList.remove("active");
        }
      }

      if (!this.querySelector('input[type="hidden"]').hasAttribute("disabled")) {
        if (!container.classList.contains("active")) {
          container.classList.add("active");
          container.previousElementSibling.classList.add("active");
        } else {
          container.classList.remove("active");
          container.previousElementSibling.classList.remove("active");
        }
      } else {
        return;
      }
    });
  }
}

// 當使用多個 Multi Select Tag 時就要初始化多個
function rpaTagsClick1(evt) {
  let rpaRoleSelectValue = evt.getAttribute("data");
  dropdownTags1.addTags(rpaRoleSelectValue);

  let rpaMultiSelectTags = document.getElementById("multiselect-with-tags-1");

  let rpaListItem = rpaMultiSelectTags.getElementsByTagName("li");

  for (i = 0; i < rpaListItem.length; i++) {
    txtValue = rpaListItem[i].textContent || rpaListItem[i].innerText;
    rpaListItem[i].style.display = "";
  }
}

// 當使用多個 Multi Select Tag 時就要初始化多個
function rpaTagsClick2(evt) {
  let rpaRoleSelectValue = evt.getAttribute("data");
  dropdownTags2.addTags(rpaRoleSelectValue);

  let rpaMultiSelectTags = document.getElementById("multiselect-with-tags-2");

  let rpaListItem = rpaMultiSelectTags.getElementsByTagName("li");

  for (i = 0; i < rpaListItem.length; i++) {
    txtValue = rpaListItem[i].textContent || rpaListItem[i].innerText;
    rpaListItem[i].style.display = "";
  }
}

// 當使用多個 Multi Select Tag 時就要初始化多個
function rpaTagsClick3(evt) {
  let rpaRoleSelectValue = evt.getAttribute("data");
  dropdownTags3.addTags(rpaRoleSelectValue);

  let rpaMultiSelectTags = document.getElementById("multiselect-with-tags-3");

  let rpaListItem = rpaMultiSelectTags.getElementsByTagName("li");

  for (i = 0; i < rpaListItem.length; i++) {
    txtValue = rpaListItem[i].textContent || rpaListItem[i].innerText;
    rpaListItem[i].style.display = "";
  }
}

// 當使用多個 Multi Select Tag 時就要初始化多個
function rpaTagsClick4(evt) {
  let rpaRoleSelectValue = evt.getAttribute("data");
  dropdownTags4.addTags(rpaRoleSelectValue);

  let rpaMultiSelectTags = document.getElementById("multiselect-with-tags-3");

  let rpaListItem = rpaMultiSelectTags.getElementsByTagName("li");

  for (i = 0; i < rpaListItem.length; i++) {
    txtValue = rpaListItem[i].textContent || rpaListItem[i].innerText;
    rpaListItem[i].style.display = "";
  }
}

rpaMultiSelectInit();
